package com.utilitarian.utils.base.file.io;

import org.junit.*;
import org.junit.runners.MethodSorters;

import java.io.File;
import java.io.IOException;
import java.net.URI;

/**
 * This class represents the corresponding test to
 * the FileUtils class
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class FileUtilsTest {


    private URI fileUri;

    private static URI dirUri;

    private URI invalidPathUri;

    private String dirPath;

    private String outputFileName;

    private String file1;

    private String file2;

    @Before
    public void setup()throws Exception{
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource("input_file").getFile());
        dirPath = file.getParentFile().getAbsolutePath();
        fileUri = file.toURI();
        dirUri = file.getParentFile().toURI();
        String invalidPath = "/test/data";
        invalidPathUri = URI.create(invalidPath);
        outputFileName = dirPath + File.separator + "merged.txt";
        file1 = classLoader.getResource("input_file").getFile();
        file2 = classLoader.getResource("sunny.txt").getFile();
    }

    @Test
    public void test10FileSizeBytes(){
        String size = FileUtils.getSizeOf(fileUri,SizeOptions.BYTES);
        Assert.assertEquals(size, "2037162");
    }

    @Test
    public void test11FileSizeDefault(){
        String size = FileUtils.getSizeOf(fileUri,null);
        Assert.assertEquals(size, "2037162");
    }

    @Test
    public void test12FileSizeKiloBytes(){
        String size = FileUtils.getSizeOf(fileUri,SizeOptions.KILOBYTES);
        Assert.assertEquals(size, "1989.0");
    }

    @Test
    public void test13FileSizeMegaBytes(){
        String size = FileUtils.getSizeOf(fileUri,SizeOptions.MEGABYTES);
        Assert.assertEquals(size, "1.9423828125");
    }

    @Test
    public void test14FileSizeGigaBytes(){
        String size = FileUtils.getSizeOf(fileUri,SizeOptions.GIGABYTES);
        Assert.assertEquals(size, "0.0018968582153320312");
    }

    @Test
    public void test15DirSize(){
        String size = FileUtils.getSizeOf(dirUri,SizeOptions.GIGABYTES);
        Assert.assertEquals(size, null);
    }

    @Test
    public void test16InvalidFilePathNull(){
        String size = FileUtils.getSizeOf(null,SizeOptions.GIGABYTES);
        Assert.assertEquals(size, null);
    }

    @Test
    public void test17InvalidFilePath(){
        String path = "c/dummy";
        File file = new File(path);
        String size = FileUtils.getSizeOf(file.toURI(),SizeOptions.GIGABYTES);
        Assert.assertEquals(size, null);
    }


    @Test
    public void test18DirSizeActualBytes()throws IOException {
        String size = FileUtils.getSizeOfDirectory(dirUri,SizeOptions.BYTES);
        Assert.assertTrue(size.startsWith( "40"));
    }

    @Test
    public void test19DirSizeActualKiloBytes()throws IOException {
        String size = FileUtils.getSizeOfDirectory(dirUri,SizeOptions.KILOBYTES);
        Assert.assertTrue(size.startsWith("39"));
    }

    @Test
    public void test20DirSizeActualMegaBytes()throws IOException {
        String size = FileUtils.getSizeOfDirectory(dirUri,SizeOptions.MEGABYTES);
        Assert.assertTrue(size.startsWith("3."));
    }

    @Test
    public void test21DirSizeActualGigaBytes()throws IOException {
        String size = FileUtils.getSizeOfDirectory(dirUri,SizeOptions.GIGABYTES);
        Assert.assertTrue(size.startsWith("0.00379"));
    }

    @Test
    public void test22DirSizeActualNullPath()throws IOException {
        String size = FileUtils.getSizeOfDirectory(null,SizeOptions.KILOBYTES);
        Assert.assertEquals(size, null);
    }

    @Test
    public void test23DirSizeActualNullSizeOption()throws IOException {
        String size = FileUtils.getSizeOfDirectory(dirUri,null);
        Assert.assertTrue(size.startsWith("407"));
    }

    @Test
    public void test24DirSizeActualInvalidFilePathUri()throws IOException {
        try {
            String size = FileUtils.getSizeOfDirectory(invalidPathUri, SizeOptions.BYTES);
        } catch(IllegalArgumentException e){
            Assert.assertNotNull(e);
        }

    }

    @Test
    public void test25TextFileGeneration() throws IOException {
        String file = FileUtils.generateFile(FileType.TEXT,1000,SizeOptions.BYTES,dirPath);
        System.out.println(file);
        File fileObj = new File(file);
        Assert.assertNotNull(fileObj);
        Assert.assertTrue(fileObj.isFile());
        Assert.assertTrue(fileObj.length() >= 1000);
        Assert.assertTrue(fileObj.getName().contains("gen"));
    }

    @Test
    public void test26TextFileGenerationKiloBytes() throws IOException {
        String file = FileUtils.generateFile(FileType.TEXT,10,SizeOptions.KILOBYTES,dirPath);
        System.out.println(file);
        File fileObj = new File(file);
        Assert.assertNotNull(fileObj);
        Assert.assertTrue(fileObj.isFile());
        Assert.assertTrue(fileObj.length() >= 10000);
        Assert.assertTrue(fileObj.getName().contains("gen"));
    }

    @Test
    public void test27TextFileGenerationMegaBytes() throws IOException {
        String file = FileUtils.generateFile(FileType.TEXT,5,SizeOptions.MEGABYTES,dirPath);
        System.out.println(file);
        File fileObj = new File(file);
        Assert.assertNotNull(fileObj);
        Assert.assertTrue(fileObj.isFile());
        Assert.assertTrue(fileObj.length() >= 5000000);
        Assert.assertTrue(fileObj.getName().contains("gen"));
    }

    @Test
    public void test28TextFileGenerationGigaBytes() throws IOException {
        String file = FileUtils.generateFile(FileType.TEXT,1,SizeOptions.GIGABYTES,dirPath);
        System.out.println(file);
        File fileObj = new File(file);
        Assert.assertNotNull(fileObj);
        Assert.assertTrue(fileObj.isFile());
        Assert.assertTrue(fileObj.length() >= 1073741824);
        Assert.assertTrue(fileObj.getName().contains("gen"));
    }

    @Test
    public void test29TextFileGenerationGigaBytesInvalid() throws IOException {
        try {
            String file = FileUtils.generateFile(FileType.TEXT, 2, SizeOptions.GIGABYTES, dirPath);
        } catch(IllegalArgumentException e){
            Assert.assertNotNull(e);
        }
    }

    @Test
    public void test30TextFileGenerationNullSizeOptions() throws IOException {
        try {
            String file = FileUtils.generateFile(FileType.TEXT, 2, SizeOptions.GIGABYTES, dirPath);
        } catch(IllegalArgumentException e){
            Assert.assertNotNull(e);
        }
    }

    @Test
    public void test31TextFileGenerationNullFileType() throws IOException {
        try {
            String file = FileUtils.generateFile(null, 2, SizeOptions.GIGABYTES, dirPath);
        } catch(IllegalArgumentException e){
            Assert.assertNotNull(e);
        }
    }

    @Test
    public void test32TextFileGenerationInvalidDir() throws IOException {
        try {
            String file = FileUtils.generateFile(FileType.TEXT, 1, SizeOptions.MEGABYTES,
                    "nonexistent/nonexistent");
        } catch(IllegalArgumentException e){
            Assert.assertNotNull(e);
        }
    }

    @Test
    public void test33MergeFiles() throws IOException {
        FileUtils.mergeFiles(outputFileName,file1,file2);
        File outputFile = new File(outputFileName);
        Assert.assertTrue(outputFile.isFile());
        Assert.assertTrue(outputFile.length() > 0);
    }

    @AfterClass
    public static void cleanUp(){
        File dir = new File(dirUri);
        File[] files = dir.listFiles();
        if(files != null){
            for(File file : files){
                file.delete();
            }
        }
    }

}
