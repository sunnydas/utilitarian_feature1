package com.utilitarian.utils.base.file.io;

import groovyjarjarantlr.StringUtils;

import java.io.*;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This class represents a collection of commonly used file based utilities that can be used for
 * various operations.
 *
 * @author Sunny Das
 */
public class FileUtils {

    private static Logger logger = Logger.getLogger(FileUtils.class.getName());


    private static String sampleText = "Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exercitation ulliam corper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem veleum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel willum lunombro dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.\n" +
            "\n";

    /**
     * Calculate size of a given file. The options are:
     *  BYTES,
     *  KILOBYTES,
     *  MEGABYTES,
     *  GIGABYTES
     * The default option is BYTES.
     * the method expects file path uri.
     *
     * @param filePathUri
     * The file path represented as an URI.
     * @param sizeOptions
     * The enum representing the options:
     * BYTES,
     * KILOBYTES,
     * MEGABYTES,
     * GIGABYTES
     * @return size
     * The size represented as a string.
     */
    public static String getSizeOf(URI filePathUri, SizeOptions sizeOptions){
        String size = null;
        if(filePathUri == null){
            logger.log(Level.SEVERE,"Null file path uri ");
            return size;
        }
        File file = new File(filePathUri);
        if(sizeOptions == null){
            logger.log(Level.WARNING,"Using default size option as bytes, since nothing specified");
            sizeOptions = SizeOptions.BYTES;
        }
        if(file.exists() && file.isFile()){
            long lengthInBytes = file.length();
            size = getSizeInternal(sizeOptions, lengthInBytes);
        }
        else{
            logger.log(Level.SEVERE,"file = " + filePathUri + ": exists = " + file.exists() + " : is file = "
            + file.isFile());
        }

        return size;
    }

    private static String getSizeInternal(SizeOptions sizeOptions, long lengthInBytes) {
        String size;
        switch (sizeOptions){
            case KILOBYTES:
                size = String.valueOf(convertBytesToKiloBytes(lengthInBytes));
                break;
            case MEGABYTES:
                size = String.valueOf(convertBytesToMegaBytes(lengthInBytes));
                break;
            case GIGABYTES:
                size = String.valueOf(convertBytesToGigaBytes(lengthInBytes));
                break;
             default:
                 size = String.valueOf(lengthInBytes);
        }
        return size;
    }

    /**
     * This method tries to fetch the cumulative size of a directory, given a directory path uri.
     * The options for size are:
     * BYTES,
     * KILOBYTES,
     * MEGABYTES,
     * GIGABYTES
     * @param dirPath
     * The directory path uri
     * @param sizeOptions
     * Size options:
     * BYTES,
     * KILOBYTES,
     * MEGABYTES,
     * GIGABYTES
     * @return size
     * The size represented as a string.
     * @throws IOException
     * Thrown if some issue occurs while recusively traversing the folder structure.
     */
    public static String getSizeOfDirectory(URI dirPath,SizeOptions sizeOptions)throws IOException {
        String size = null;
        if(dirPath == null){
            logger.log(Level.SEVERE,"Dir path ur is null");
            return size;
        }
        if(sizeOptions == null){
            logger.log(Level.WARNING,"Using default size option as bytes, since nothing specified");
            sizeOptions = SizeOptions.BYTES;
        }
        File file = new File(dirPath);
        if(file.exists()
                && file.isDirectory()){
            Path path = Paths.get(dirPath);
            long sizeInBytes = Files.walk(path)
                    .filter(p -> p.toFile().isFile())
                    .mapToLong(p -> p.toFile().length())
                    .sum();
            size = getSizeInternal(sizeOptions,sizeInBytes);
        }
        return size;
    }


    public static long getSizeInBytes(long size,SizeOptions sizeOptions){
        if(sizeOptions != null
                && !sizeOptions.equals(SizeOptions.BYTES)) {
            switch (sizeOptions) {
                case GIGABYTES:
                    size *= 1024;
                case MEGABYTES:
                    size *= 1024;
                case KILOBYTES:;
                    size *= 1024;
                case BYTES:
                    break;
            }
        }
        return size;
    }

    /**
     * Generate a file with some text for a given size. This could be useful for testing
     *  purposes. Please note 1 GB is the max limit. Please note that tis is an approximation
     *  and we use ~555KB based blocks to generate the file.
     *
     *
     * @param fileType
     * @param expectedSize
     * @param sizeOptions
     * @param directory
     * @return
     * @throws IOException
     */
    public static String generateFile(FileType fileType,long expectedSize,
                                    SizeOptions sizeOptions,
                                    String directory) throws IOException {
        String generatedFile = null;
        if(fileType != null && sizeOptions != null
           && directory != null){
            expectedSize = getSizeInBytes(expectedSize,sizeOptions);
            if(expectedSize > 1073741824L){
                String err = "We cannot expect this tool to create a file of more than 1 GB";
                logger.log(Level.SEVERE,err);
                throw new IllegalArgumentException(err);
            }
            String uuid = String.valueOf(System.currentTimeMillis());
            File file = new File(directory);
            if(file.isDirectory()){
                StringBuilder generatedFileBuilder = new StringBuilder(directory);
                generatedFileBuilder.append(File.separator);
                generatedFileBuilder.append("gen_");
                generatedFileBuilder.append(uuid);
                if(fileType.equals(FileType.TEXT)){
                    generatedFileBuilder.append(".txt");
                    generatedFile = generatedFileBuilder.toString();
                    File genFileObj = new File(generatedFile);
                    boolean created = genFileObj.createNewFile();
                    if(created){
                        try(BufferedWriter bufferedWriter
                                    = new BufferedWriter(new FileWriter(genFileObj))){
                            while(genFileObj.length() < expectedSize){
                                bufferedWriter.write(sampleText);
                                bufferedWriter.flush();
                            }
                        }
                    }
                }
            }
            else{
                String err = "Invalid directory specified = " + directory;
                logger.log(Level.SEVERE,err);
                throw new IllegalArgumentException(err);
            }
        }
        else{
            String err = "One or more of the mandatory parameters are" +
                    " null: fileType = " + fileType + " sizeOptions = " + sizeOptions + " directory = "
                    + directory;
            logger.log(Level.SEVERE,err);
            throw new IllegalArgumentException(err);
        }
        return generatedFile;
    }


    /**
     *
     * @param inputStreams
     */
    private static void closeInputStream(List<InputStream> inputStreams){
        if(inputStreams != null){
            for(InputStream inputStream : inputStreams){
                try {
                    inputStream.close();
                } catch (IOException e) {
                    logger.warning(e.getMessage());
                }
            }
        }
    }


    /**
     * Merges given set of input files into a single output file.
     *
     *
     * @param outputFileName
     * @param fileNames
     * @throws IOException
     */
    public static void mergeFiles(String outputFileName,String ... fileNames) throws IOException {
        if(outputFileName != null
                && fileNames != null
                && fileNames.length > 0){
            List<InputStream> fises = new ArrayList<>(fileNames.length);
            try {
                logger.info("Begin merge process for input files");
                for (int i = 0; i < fileNames.length; i++) {
                    fises.add(new FileInputStream(new File(fileNames[i])));
                }
                try (SequenceInputStream sequenceInputStream =
                             new SequenceInputStream(Collections.enumeration(fises));
                    FileOutputStream fileOutputStream = new FileOutputStream(new File(outputFileName))) {
                    int byteRead;
                    while ((byteRead = sequenceInputStream.read()) != -1) {
                        fileOutputStream.write(byteRead);
                        fileOutputStream.flush();
                    }
                    logger.info("Merge complete to output file = " + outputFileName);
                }
            }finally{
                closeInputStream(fises);
            }
        }
        else{
            logger.warning("Invalid parameters passed:  outputFileName " +
                    outputFileName + " fileNames " + fileNames);
        }
    }

    public static double convertBytesToKiloBytes(long bytes){
        return (bytes/1024);
    }

    public static double convertBytesToMegaBytes(long bytes){
        return (convertBytesToKiloBytes(bytes)/1024);
    }

    public static double convertBytesToGigaBytes(long bytes){
        return (convertBytesToMegaBytes(bytes)/1024);
    }





}
