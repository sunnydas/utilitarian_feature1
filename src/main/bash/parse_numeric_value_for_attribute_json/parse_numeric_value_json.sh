#!/usr/bin/env bash

#########################################################################
# @description: This script can be used to extract and print numeric    # 
# value for a json file.                                                #  
# @author: Sunny Das                                                    #
# @license: MIT                                                         #
#########################################################################


extract_attribute(){
  jsonFileName="$OPTARG"
  if [ -z "$jsonFileName" ];then
    echo "Invalid json file name specified."
	exit 1
  fi
  if [ ! -f "$jsonFileName" ];then
    echo "Json file = $jsonFileName not found."
	exit 1
  fi
  read -p "Enter attribute which needs parsing: " key
  if [ -z "$key" ];then
    echo "Invalid input attribute"
	exit 1 
  fi
  value=`cat $jsonFileName | grep \"$key\": | grep -Po [0-9]+`
  echo "Extracted value:"
  echo $value  
}


help(){
   echo "===================================="
   echo "Extract numeric attribute from json file for a given key"
   echo "Usage: ./parse_numeric_value_json.sh -f <json_file_name>"
}


OPTIND=1
while getopts "h?f:" opt; do
    case "$opt" in
    h|\?)
        help
        exit 0
        ;;
    f) 
     extract_attribute $OPTARG
     exit 0
     ;; 
    *)
	help
    shift
    ;;      
    esac
done
shift $((OPTIND-1))







